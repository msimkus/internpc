﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InternPC.Models;

namespace InternPC.Services
{
    public class CompanyRepository : ICompanyRepository
    {
        List<Company> companies;

        public CompanyRepository()
        {
            companies = new List<Company>();
            companies.Add(new Company() { Id = 1, Name = "Adidas" });
            companies.Add(new Company() { Id = 2, Name = "Fila" });
            companies.Add(new Company() { Id = 3, Name = "Nike" });
            companies.Add(new Company() { Id = 4, Name = "Puma" });
            companies.Add(new Company() { Id = 5, Name = "Reebok" });
        }

        public IEnumerable<Company> GetAll()
        {
            return companies;
        }

        public Company Get(int id)
        {
            var company = companies.FirstOrDefault(x => x.Id == id);

            return company;
        }

        public void Add(Company company)
        {
            companies.Add(company);
        }

        public void Remove(Company company)
        {
            companies.Remove(company);
        }

        public void Update(Company company)
        {
            Company companyToUpdate = companies.FirstOrDefault(x => x.Id == company.Id);
            companyToUpdate.Name = company.Name;
        }
    }
}
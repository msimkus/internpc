﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InternPC.Models;

namespace InternPC.Services
{
    public interface ICompanyRepository
    {
        IEnumerable<Company> GetAll();
        Company Get(int id);
        void Add(Company company);
        void Remove(Company company);
        void Update(Company company);
    }
}
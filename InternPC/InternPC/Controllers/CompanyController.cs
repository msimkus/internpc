﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InternPC.Models;
using InternPC.Services;

namespace InternPC.Controllers
{
    public class CompanyController : Controller
    {
        private ICompanyRepository _repository;

        public CompanyController(ICompanyRepository repository)
        {
            _repository = repository;
        }

        public ActionResult Index()
        {
            var companies = _repository.GetAll().ToList();

            return View(companies);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Company company = _repository.Get(id.Value);

            if (company == null)
            {
                return HttpNotFound();
            }

            return View(company);
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] Company company)
        {
            if (ModelState.IsValid)
            {
                _repository.Add(company);
                return RedirectToAction("Index");
            }

            return View(company);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Company company = _repository.Get(id.Value);

            if (company == null)
            {
                return HttpNotFound();
            }

            return View(company);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name")] Company company)
        {
            if (ModelState.IsValid)
            {
                _repository.Update(company);
                return RedirectToAction("Index");
            }

            return View(company);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Company company = _repository.Get(id.Value);

            if (company == null)
            {
                return HttpNotFound();
            }

            return View(company);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Company company = _repository.Get(id);
            _repository.Remove(company);

            return RedirectToAction("Index");
        }
    }
}

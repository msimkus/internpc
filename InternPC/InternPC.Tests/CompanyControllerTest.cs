﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using InternPC.Controllers;
using InternPC.Models;
using InternPC.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace InternPC.Tests
{
    [TestClass]
    public class CompanyControllerTest
    {
        Mock<ICompanyRepository> _repository;
        CompanyController controller;
        List<Company> companies;

        [TestInitialize]
        public void Initialize()
        {
            _repository = new Mock<ICompanyRepository>();
            controller = new CompanyController(_repository.Object);
            companies = new List<Company>() {
                new Company(){ Id = 1, Name = "Apple" },
                new Company(){ Id = 2, Name = "Google" },
                new Company(){ Id = 3, Name = "Microsoft" }
            };
        }

        [TestMethod]
        public void Company_Get_All()
        {
            // Arrange
            _repository.Setup(x => x.GetAll()).Returns(companies);

            // Act
            var result = controller.Index() as ViewResult;
            var model = result.ViewData.Model as List<Company>;

            // Assert
            Assert.AreEqual(3, model.Count);

            Assert.AreEqual(1, model[0].Id);
            Assert.AreEqual("Apple", model[0].Name);
            Assert.AreEqual(2, model[1].Id);
            Assert.AreEqual("Google", model[1].Name);
            Assert.AreEqual(3, model[2].Id);
            Assert.AreEqual("Microsoft", model[2].Name);
        }

        [TestMethod]
        public void Company_Get_By_Id()
        {
            // Arrange
            _repository.Setup(x => x.Get(companies[2].Id)).Returns(new Company() { Id = companies[2].Id, Name = companies[2].Name });

            // Act
            Company result = (Company)(controller.Details(companies[2].Id) as ViewResult).ViewData.Model;

            // Assert
            Assert.AreEqual(companies[2].Id, result.Id);
            Assert.AreEqual(companies[2].Name, result.Name);
        }

        [TestMethod]
        public void Company_Create_Valid()
        {
            // Arrange
            Company company = new Company() { Name = "Intel" };

            // Act
            var result = (RedirectToRouteResult)controller.Create(company);

            // Assert
            _repository.Verify(x => x.Add(company), Times.Once);
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [TestMethod]
        public void Company_Create_Invalid()
        {
            // Arrange
            Company company = new Company() { Name = "" };
            controller.ModelState.AddModelError("Error", "Something went wrong");

            // Act
            var result = (ViewResult)controller.Create(company);

            // Assert
            _repository.Verify(x => x.Add(company), Times.Never);
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void Company_Edit()
        {
            // Arrange
            _repository.Setup(x => x.Get(companies[0].Id)).Returns(new Company() { Id = companies[0].Id, Name = companies[0].Name });

            // Act
            Company result = (Company)(controller.Edit(companies[0].Id) as ViewResult).ViewData.Model;

            // Assert
            Assert.AreEqual(companies[0].Id, result.Id);
            Assert.AreEqual(companies[0].Name, result.Name);
        }

        [TestMethod]
        public void Company_Delete()
        {
            // Arrange
            _repository.Setup(x => x.Get(companies[1].Id)).Returns(new Company() { Id = companies[1].Id, Name = companies[1].Name });

            // Act
            Company result = (Company)(controller.Delete(companies[1].Id) as ViewResult).ViewData.Model;

            // Assert
            Assert.AreEqual(companies[1].Id, result.Id);
            Assert.AreEqual(companies[1].Name, result.Name);
        }
    }
}